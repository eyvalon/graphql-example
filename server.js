const { ApolloServer } = require("apollo-server");
const glue = require("schemaglue");
// const { importSchema } = require("graphql-import");
// const typeDefs = importSchema("./src/graphql/schemas/random.graphql");

const RandomUser = require("./src/datasources/randomUser.js");
const { schema, resolver } = glue("src/graphql");

const server = new ApolloServer({
  typeDefs: schema,
  resolvers: resolver,
  playground: {
    settings: {
      "editor.theme": "light"
    }
  },
  dataSources: () => ({
    RandomUser: new RandomUser()
  })
});

server.timeout = 0;
server.listen({ port: 4000 }).then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
