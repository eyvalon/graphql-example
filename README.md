**Learning resources to get started with GraphQL**:

- [A Practical GraphQL Getting Started Guide with Node.js and Express](https://www.digitalocean.com/community/tutorials/a-practical-graphql-getting-started-guide-with-nodejs)

- [GraphQL: Schema, Resolvers, Type System, Schema Language, and Query Language](https://www.telerik.com/blogs/graphql-schema-resolvers-type-system-schema-language-query-language)

- [How to use Apollo Datasource to call a Rest API — GraphQL](https://medium.com/become-developer/how-to-use-apollo-datasource-to-call-a-rest-api-graphql-65602607ceef)

- [Easier GraphQL wrappers for your REST API’s](https://medium.com/yld-blog/easier-graphql-wrappers-for-your-rest-apis-1410b0b5446d)
