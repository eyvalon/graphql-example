// Sample users
const users = [
  {
    id: 1,
    name: "Brian",
    age: "21",
    shark: "Great White Shark"
  },
  {
    id: 2,
    name: "Kim",
    age: "22",
    shark: "Whale Shark"
  },
  {
    id: 3,
    name: "Faith",
    age: "23",
    shark: "Hammerhead Shark"
  },
  {
    id: 4,
    name: "Joseph",
    age: "23",
    shark: "Tiger Shark"
  },
  {
    id: 5,
    name: "Joy",
    age: "25",
    shark: "Hammerhead Shark"
  }
];

exports.resolver = {
  Query: {
    users: (root, args, context, info) => {
      return users;
    },

    user: (root, args, context, info) => {
      return users.find((e) => e.id === args.id);
    }
  },
  Mutation: {
    getUpdateUser: (root, args, context, info) => {
      users.map((user) => {
        if (user.id === args.id) {
          user.name = args.name;
          user.age = args.age;
          return user;
        }
      });
    }
  }
};
