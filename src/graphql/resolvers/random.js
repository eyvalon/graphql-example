exports.resolver = {
  Query: {
    getUser: (root, args, { dataSources }) => {
      return dataSources.RandomUser.getUser(args.gender);
    },
    getUsers: (root, args, { dataSources }) => {
      return dataSources.RandomUser.getUsers(args.people, args.gender);
    }
  }
};
